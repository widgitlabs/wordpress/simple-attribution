<?php
/**
 * Core unit test
 *
 * @package     SimpleAttribution\Tests\Core
 * @since       3.0.0
 */

// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}


/**
 * Core unit tests
 *
 * @since       3.0.0
 */
class Tests_Simple_Attribution extends WP_UnitTestCase {


	/**
	 * Test suite object
	 *
	 * @access      protected
	 * @since       3.0.0
	 * @var         object $object The test suite object
	 */
	protected $object;


	/**
	 * Set up this test suite
	 *
	 * @access      public
	 * @since       3.0.0
	 * @return      void
	 */
	public function setUp() {
		parent::setUp();
		$this->object = Simple_Attribution();
	}


	/**
	 * Tear down this test suite
	 *
	 * @access      public
	 * @since       3.0.0
	 * @return      void
	 */
	public function tearDown() { // phpcs:ignore Generic.CodeAnalysis.UselessOverridingMethod
		parent::tearDown();
	}


	/**
	 * Test the Simple_Attribution instance
	 *
	 * @access      public
	 * @since       3.0.0
	 * @return      void
	 */
	public function test_simple_attribution_instance() {
		$this->assertClassHasStaticAttribute( 'instance', 'Simple_Attribution' );
	}


	/**
	 * Test constants
	 *
	 * @access      public
	 * @since       3.0.0
	 * @return      void
	 */
	public function test_constants() {
		// Make SURE I updated the version.
		$this->assertSame( SIMPLE_ATTRIBUTION_VER, '2.1.2' );

		// Plugin folder URL.
		$path = str_replace( 'tests/', '', plugin_dir_url( __FILE__ ) );
		$this->assertSame( SIMPLE_ATTRIBUTION_URL, $path );

		// Plugin folder path.
		$path               = str_replace( 'tests/', '', plugin_dir_path( __FILE__ ) );
		$path               = substr( $path, 0, -1 );
		$simple_attribution = substr( SIMPLE_ATTRIBUTION_DIR, 0, -1 );
		$this->assertSame( $simple_attribution, $path );
	}


	/**
	 * Test includes
	 *
	 * @access      public
	 * @since       3.0.0
	 * @return      void
	 */
	public function test_includes() {
		$this->assertFileExists( SIMPLE_ATTRIBUTION_DIR . 'class-simple-attribution.php' );
		$this->assertFileExists( SIMPLE_ATTRIBUTION_DIR . 'includes/admin/settings/register-settings.php' );

		$this->assertFileExists( SIMPLE_ATTRIBUTION_DIR . 'includes/filters.php' );
		$this->assertFileExists( SIMPLE_ATTRIBUTION_DIR . 'includes/misc-functions.php' );
		$this->assertFileExists( SIMPLE_ATTRIBUTION_DIR . 'includes/scripts.php' );

		/** Check Assets Exist */
		$this->assertFileExists( SIMPLE_ATTRIBUTION_DIR . 'assets/css/simple-attribution.css' );
		$this->assertFileExists( SIMPLE_ATTRIBUTION_DIR . 'assets/css/simple-attribution.min.css' );
		$this->assertFileExists( SIMPLE_ATTRIBUTION_DIR . 'assets/js/admin.js' );
		$this->assertFileExists( SIMPLE_ATTRIBUTION_DIR . 'assets/js/admin.min.js' );
		$this->assertFileExists( SIMPLE_ATTRIBUTION_DIR . 'assets/banner-772x250.jpg' );
		$this->assertFileExists( SIMPLE_ATTRIBUTION_DIR . 'assets/banner-1544x500.jpg' );
		$this->assertFileExists( SIMPLE_ATTRIBUTION_DIR . 'assets/icon.svg' );
		$this->assertFileExists( SIMPLE_ATTRIBUTION_DIR . 'assets/icon-128x128.jpg' );
		$this->assertFileExists( SIMPLE_ATTRIBUTION_DIR . 'assets/icon-256x256.jpg' );
		$this->assertFileExists( SIMPLE_ATTRIBUTION_DIR . 'assets/images/clip-light.png' );
		$this->assertFileExists( SIMPLE_ATTRIBUTION_DIR . 'assets/images/clip.png' );
		$this->assertFileExists( SIMPLE_ATTRIBUTION_DIR . 'assets/images/clipboard-light.png' );
		$this->assertFileExists( SIMPLE_ATTRIBUTION_DIR . 'assets/images/clipboard.png' );
		$this->assertFileExists( SIMPLE_ATTRIBUTION_DIR . 'assets/images/globe-1-light.png' );
		$this->assertFileExists( SIMPLE_ATTRIBUTION_DIR . 'assets/images/globe-1.png' );
		$this->assertFileExists( SIMPLE_ATTRIBUTION_DIR . 'assets/images/globe-2-light.png' );
		$this->assertFileExists( SIMPLE_ATTRIBUTION_DIR . 'assets/images/globe-2.png' );
		$this->assertFileExists( SIMPLE_ATTRIBUTION_DIR . 'assets/images/quote-light.png' );
		$this->assertFileExists( SIMPLE_ATTRIBUTION_DIR . 'assets/images/quote.png' );
	}
}
